// R2D2
#pragma config(Sensor, S3, , sensorEV3_Color)
#pragma config(Sensor, S4, , sensorEV3_Ultrasonic)

#define LM motorC
#define RM motorB

#define Light S3
#define Sonar S4

void drawScreen(){
	eraseDisplay();
	displayCenteredBigTextLine(1, "R2D2");
	displayCenteredBigTextLine(3, "Light: %d", SensorValue[Light]);
	displayCenteredBigTextLine(4, "Sonar: %d", SensorValue[Sonar]);
}

task main()
{
	drawScreen();
	int black = 2, white=50;
	int dist = 10;
	int threshold = (black + white)/2;

	while(SensorValue[Sonar] > dist)  // check if the ultrasonic sensor detects > dist
	{
		drawScreen();
		if (SensorValue[Light] < threshold)
		{ // light sensor detects dark
			motor[RM] = -20;
			motor[LM] = 75;
		}
		else
		{ // light sensor sees bright
			motor[RM] = 75;
			motor[LM] = -20;
		}
	}
	// Rotina de desviar


	motor[RM] = motor[LM] = 0;
}