// R2D2
#pragma config(Sensor, S3, , sensorEV3_Color)
#pragma config(Sensor, S4, , sensorEV3_Ultrasonic)

#define LM motorC
#define RM motorB

#define Light S3
#define Sonar S4

task main()
{
	int black = 40, white=50;
	int dist = 10;
	int threshold = (black + white)/2;

	//SensorType[Light] = sensorLightActive;
	//SensorType[Sonar] = sensorSONAR;

	while(SensorValue[Sonar] > dist)  // check if the ultrasonic sensor detects > dist
	{
		motor[RM] = motor[LM] = 10;
		wait1Msec(300);
		if (SensorValue[Light] > threshold)
		{ // light sensor detects dark
			motor[RM] = -4;
			motor[LM] = 10;
			wait1Msec(300);
		}
		else
		{ // light sensor sees bright
			motor[RM] = 20;
			motor[LM] = -8;
			wait1Msec(300);
		}
	}
	// Rotina de desviar

	motor[RM] = motor[LM] = 0;
}
