// R2D2
#pragma config(Sensor, S3, , sensorEV3_Color)
#pragma config(Sensor, S4, , sensorEV3_Ultrasonic)

#define LM motorC
#define RM motorB

#define Light S3
#define Sonar S4

task main()
{
	int black = 10;
	int dist = 10;
	//int threshold = (black + white)/2;

	//SensorType[Light] = sensorLightActive;
	//SensorType[Sonar] = sensorSONAR;

	while(SensorValue[Sonar] > dist)  // check if the ultrasonic sensor detects > dist
	{
		if (SensorValue[Light] > black)
		{ // light sensor detects dark
			motor[RM] = 10;
			motor[LM] = -5;
		}
		else
		{ // light sensor sees bright
			motor[RM] = 8;
			motor[LM] = 10;
		}
	}
	// Rotina de desviar

	motor[RM] = motor[LM] = 0;
}
